package com.ats;
import java.io.IOException;
import java.io.InputStream;

import com.ats.flex.messaging.amf.io.AMF3Deserializer;

public class AmfTools {

	public static Object deserialize(InputStream in) throws IOException {
		final AMF3Deserializer amf3 = new AMF3Deserializer(in);

		final Object result = amf3.readObject();
		amf3.close();
		
		return result;
	}
}