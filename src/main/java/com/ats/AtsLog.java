package com.ats;

import org.apache.commons.logging.Log;

public class AtsLog implements Log {

	@Override
	public void debug(Object message) {
		System.out.println(message.toString());
	}

	@Override
	public void debug(Object message, Throwable t) {
		System.out.println(message.toString());
	}

	@Override
	public void error(Object message) {
		System.out.println(message.toString());
	}

	@Override
	public void error(Object message, Throwable t) {
		System.out.println(message.toString());
	}

	@Override
	public void fatal(Object message) {
		System.out.println(message.toString());
	}

	@Override
	public void fatal(Object message, Throwable t) {
		System.out.println(message.toString());
	}

	@Override
	public void info(Object message) {
		System.out.println(message.toString());
	}

	@Override
	public void info(Object message, Throwable t) {
		System.out.println(message.toString());
	}

	@Override
	public boolean isDebugEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isErrorEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isFatalEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isInfoEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isTraceEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isWarnEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void trace(Object message) {
		System.out.println(message.toString());
	}

	@Override
	public void trace(Object message, Throwable t) {
		System.out.println(message.toString());
	}

	@Override
	public void warn(Object message) {
		System.out.println(message.toString());
	}

	@Override
	public void warn(Object message, Throwable t) {
		System.out.println(message.toString());
	}

}
